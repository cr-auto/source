import {combineReducers, configureStore} from "@reduxjs/toolkit";
import { advCreateReducer, getAllAdsReducer, getBrandsReducer, getModelsReducer } from "./reducers/AdvertisingReducers";
import { shopCreateReducer } from "./reducers/ShopReducers";
import { userDetailsReducer, userRegisterReducer, userSigninReducer, userUpdateProfileReducer } from "./reducers/UserReducers";

const initialState = {
    userSignin: {
        userInfo: localStorage.getItem('userInfo')
        ? JSON.parse(localStorage.getItem('userInfo'))
        : null,
    }
}

const reducer = combineReducers({
/*     shop: shopCreateReducer,
    userSignin: userSigninReducer,
    userRegister: userRegisterReducer,
    userDetails: userDetailsReducer,
    userUpdateProfile: userUpdateProfileReducer, */
    advCreate: advCreateReducer,
    brands: getBrandsReducer,
    allAds: getAllAdsReducer, 
    models: getModelsReducer,
})

const store = configureStore({
    reducer,
    initialState,
    devTools: true
})
export default store;
