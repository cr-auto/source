import axios from "axios";
import { ADVERTISING_CREATE_REQUEST, ADVERTISING_CREATE_SUCCESS, ADVERTISING_CREATE_FAIL, GET_BRANDS_REQUEST, GET_BRANDS_SUCCESS, GET_BRANDS_FAIL, ADVERTISING_GET_ALL_REQUEST, ADVERTISING_GET_ALL_SUCCESS, ADVERTISING_GET_ALL_FAIL, GET_MODELS_REQUEST, GET_MODELS_SUCCESS, GET_MODELS_FAIL } from "../constants/AdvertisingConstants";

export const getAllAds = () => async (dispatch) => {
    dispatch({ type: ADVERTISING_GET_ALL_REQUEST });

    try {
        const { data } = await axios.get('http://synequit.cz/api/Inzerat/GetAllAds');
        dispatch({ type: ADVERTISING_GET_ALL_SUCCESS, payload: data });
    } catch (error) {
        dispatch({ type: ADVERTISING_GET_ALL_FAIL, payload: error.message })
    }
}

export const createAdv = (adv) => async (dispatch) => {
    dispatch({ type: ADVERTISING_CREATE_REQUEST, payload: adv });
    try {
        console.log(adv);
        const { data } = await axios.post('http://synequit.cz/api/Inzerat/SaveAd', adv, {
            headers: { 
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
        },
        });
        dispatch({ type: ADVERTISING_CREATE_SUCCESS, payload: data });
    } catch (error) {
        dispatch({ type: ADVERTISING_CREATE_FAIL, payload: error.message })
    }
}

export const getBrands = () => async (dispatch) => {
    dispatch({ type: GET_BRANDS_REQUEST });
    try {
        const { data } = await axios.get('http://synequit.cz/api/Inzerat/GetAllBrands');
        dispatch({ type: GET_BRANDS_SUCCESS, payload: data });
    } catch (error) {
        const message =
            error.response && error.response.data.message
            ? error.response.data.message
            : error.message;
        dispatch({ type: GET_BRANDS_FAIL, payload: message});
    }
}

export const getModels = (id) => async (dispatch) => {
    dispatch({ type: GET_MODELS_REQUEST });
    try {
        const { data } = await axios.get('http://synequit.cz/api/Inzerat/GetAllModelsForBrand', id);
        dispatch({ type: GET_MODELS_SUCCESS, payload: data });
    } catch (error) {
        const message =
            error.response && error.response.data.message
            ? error.response.data.message
            : error.message;
        dispatch({ type: GET_MODELS_FAIL, payload: message});
    }
}