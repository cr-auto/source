import { SHOP_CREATE_FAIL, SHOP_CREATE_REQUEST, SHOP_CREATE_SUCCESS } from "../constants/ShopConstants";

export const createCarAd = () => async (dispatch, getState) => {
    dispatch ({ type: SHOP_CREATE_REQUEST });
    /*const {
        userSignin: {userInfo}
    } = getState();*/
    try {
        const { data } = await Axios.post("/api/cars", {}, {
            //headers: { Authorization: `Bearer ${userinfo.token}`},
        });
        dispatch({
            type: SHOP_CREATE_SUCCESS,
            payload: data.carAd,
        });
    } catch (error) {
        const message = 
            error.response && error.response.data.message
            ? error.response.data.message
            : error.message;
        dispatch({ type: SHOP_CREATE_FAIL, payload: message });
    }
};