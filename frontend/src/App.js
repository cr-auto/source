import React from "react";
import { Route, Routes, BrowserRouter } from "react-router-dom";

import Navbar from "./Components/Navbar";
import ShopDetail from "./Screen/ShopDetail";
import Shop from "./Screen/Shop";
import MainScreen from "./Screen/MainScreen";
import Footer from "./Components/Footer";
import "./App.css";
import Advertisment from "./Screen/Advertisment";
import Login from "./Screen/Login";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/shop/:id" element={<ShopDetail />} exact></Route>
          <Route path="/shop" element={<Shop />}></Route>
          <Route path="/advertising" element={<Advertisment />}></Route>
          <Route path="/login" element={<Login />}></Route>
          <Route path="/" element={<MainScreen />}></Route>
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  );
};

export default App;
