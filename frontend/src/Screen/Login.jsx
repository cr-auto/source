import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { signin } from '../actions/UserActions';

import "./Login.css";

const Login = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

/*   const redirect = props.location.search
    ? props.location.search.split('=')[1]
    : '/'; */

  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo, loading, error } = userSignin;

  const dispatch = useDispatch();
  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(signin(email, password));
  }

/*   useEffect(() => {
    if (userInfo) {
      props.history.push(redirect);
    }
  }, [props.history, redirect, userInfo]) */

  return (
    <div className="login__container">
      <div className="login__text">
        <div className="login__headline">
          <h1>Přihlásit se</h1>
        </div>
        <div className="login__content">
          <div>
            <p>
              Připravili jsme pro vás Nemo enim ipsam voluptatem quia voluptas
              sit aspernatur aut fugit, sed quia consequuntur magni dolores eos
              qui ratione.
            </p>
          </div>
          <div className="login__footer__section">
            <strong>Nemáte ještě účet?</strong>
            <button>Registrovat se</button>
          </div>
        </div>
      </div>
      <div className="login__form">
        <div className="login__headline">
          <h1>Máte již účet?</h1>
        </div>
        <form className="login__form__content" onSubmit={submitHandler}>
          <div className="login__input__section">
            <label htmlFor="email">E-mail: *</label>
            <input 
              type="email" 
              id="email" 
              required 
              onChange={(e) => setEmail(e.target.value)} />
          </div>
          <div className="login__input__section">
            <label htmlFor="password">Heslo: *</label>
            <input
              type="password"
              id="password"
              required
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <div className="login__under__input__section__left">
              <button type="submit">Přihlásit</button>
            </div>
            <div className="login__under__input__section__right">
              <strong>Zapomenuté heslo?</strong>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
