import React, { useRef, useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from 'react-router-dom';

import "./ShopDetail.css";
import images from "../constants/images";
import { FaGasPump, FaPlus } from "react-icons/fa";
import { createAdv, getBrands, getModels } from "../actions/AdvertisingActions";
import { ADVERTISING_CREATE_RESET } from "../constants/AdvertisingConstants";
import SelectSearch, { fuzzySearch } from "react-select-search";

const Advertisment = (props) => { 
  const searchInput = useRef();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const brandsList = useSelector((state) => state.brands);
  const { loading, error, data } = brandsList;

  const modelsList = useSelector((state) => state.models);
  const { loading: modelLoading, error: modelError, data: modelData } = modelsList;

  const createdAdv = useSelector((state) => state.advCreate);
  const {loading: advLoading, error: advError, adv, success} = createdAdv;

  const [brand, setBrand] = useState();
  const [model, setModel] = useState();
  const [year, setYear] = useState();
  const [price, setPrice] = useState();
  const [kilometers, setKilometers] = useState();
  const [info, setInfo] = useState();
  const [name, setName] = useState();
  const [phoneNumber, setPhoneNumber] = useState();
  const [email, setEmail] = useState();
  const [message, setMessage] = useState();
  const [city, setCity] = useState();
  const [fuel, setFuel] = useState();
  const [transmission, setTransmission] = useState();

  useEffect(() => {
    if (success) {
      dispatch({ type: ADVERTISING_CREATE_RESET});
      navigate('/shop');
    }
    dispatch(getBrands());
    if (brand) {
      dispatch(getModels(brand));
    }
  }, [dispatch, brand, success])

  const brandOptions = [];
  const modelOptions = [];
  const fuelOptions = [
    {name: 'gas',  value: 1},
    {name: 'naphtha',  value: 2},
    {name: 'lpg',  value: 3},
  ];
  const transOptions = [
    {name: 'automatic', value: 1},
    {name: 'manual', value: 2}
  ]

  data && data.forEach(singleBrand => {
    brandOptions.push({name: singleBrand.znackaVozu, value: singleBrand.id})
  });

  modelData && modelData.forEach(singleModel => {
    modelOptions.push({name: singleModel.znackaVozu, value: singleModel.id})
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    const adv = {
      znacka: brand ? brand.toString() : null,
      model: 'model',
      cena: price,
      rokVyroby: year,
      najeteKm: kilometers,
      palivo: fuel ? fuel.toString() : null,
      prevodovka: transmission ? transmission.toString() : null,
      dodatecneInfo: message,
      jmeno: name,
      email: email,
      telCislo: phoneNumber,
      location: city,
      adMessage: info
    };
    dispatch(createAdv(adv));
  }

  return ( 
    <div className="shop-detail__container"> 
      <div className="shop-detail__section-form">
        <div className="shop-detail__intro-wrapper">
          <h1>Nový inzerát</h1>
          {/* <div>
            <label htmlFor="cebia">Chcete použít Cebii?</label>
            <input
              id="cebia"
              type="checkbox"
              defaultChecked={cebia}
              onChange={(e) => setCebia(e.target.checked)}
            />
          </div> */}
          {/* <p>Prosím vyplňte pečlivě následující tabulku</p> */}
        </div>
          <form onSubmit={handleSubmit}>
            <div className="shop-detail__form__inputs-section">
              <div className="shop-detail__form__inputs-upper__section-first">
                <div className="shop-detail__form__inputs_dropdowns">
                  <SelectSearch
                    ref={searchInput} 
                    options={brandOptions} 
                    search 
                    name='brands' 
                    placeholder="Hledat značku" 
                    filterOptions={fuzzySearch}
                    value={brand}
                    onChange={setBrand}  
                  />

                  <SelectSearch
                    ref={searchInput} 
                    options={modelOptions} 
                    search 
                    name='models' 
                    placeholder="Hledat model" 
                    filterOptions={fuzzySearch}
                    value={model}
                    onChange={setModel}  
                  />
                  
                  <SelectSearch
                    ref={searchInput} 
                    options={fuelOptions} 
                    search 
                    name='fuel' 
                    placeholder="Hledat palivo" 
                    filterOptions={fuzzySearch}
                    value={fuel}
                    onChange={setFuel}  
                  />
                  
                  <SelectSearch
                    ref={searchInput} 
                    options={transOptions} 
                    search 
                    name='models' 
                    placeholder="Hledat převodovku" 
                    filterOptions={fuzzySearch}
                    value={transmission}
                    onChange={setTransmission}  
                  />
                </div>
                <input
                  type="text"
                  id="year"
                  placeholder="Rok výroby"
                  value={year}
                  onChange={(e) => setYear(e.target.value)}
                />

                <div className="shop-detail__form__inputs-upper__section-second">
                  <input
                    type="text"
                    id="kilometres"
                    placeholder="Najeté Kilometry"
                    value={kilometers}
                    onChange={(e) => setKilometers(e.target.value)}
                  />
                  <input
                    className="shop-detail__form__inputs-section-second__input"
                    type="text"
                    placeholder="Doplňující informace"
                    value={info}
                    onChange={(e) => setInfo(e.target.value)}
                  />
                </div>
                <div className="shop-detail__form__inputs-upper__section-third">
                  <div className="rectangular">
                    <div className="rectangular-img">
                      <img src={images.wv} alt="car"></img>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="shop-detail__form__inputs-section-middle">
              <div className="shop-detail__form__inputs-middle__section-first h-auto">
                <div className="shop-detail__form__inputs-label__section">
                  <label htmlFor="name">Vaše celé jméno</label>
                  <input
                    id="name"
                    type="text"
                    placeholder="Pavel Novák"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                </div>
                <div className="shop-detail__form__inputs-label__section">
                  <label htmlFor="phone">Telefon</label>
                  <input
                    id="phone"
                    type="text"
                    placeholder="+420 731 695 368"
                    value={phoneNumber}
                    onChange={(e) => setPhoneNumber(e.target.value)}
                  />
                </div>
                <div className="shop-detail__form__inputs-label__section">
                  <label htmlFor="email">Email</label>
                  <input
                    id="email"
                    type="text"
                    placeholder="NovakP@seznam.cz"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>
              </div>
              <div className="shop-detail__form__inputs-middle__section_second">
                <div className="shop-detail__form__inputs-label__section h-100">
                  <label htmlFor="message">
                    Zde nám zanechte zprávu či dotaz
                  </label>
                  <textarea
                    className="h-100"
                    id="message"
                    value={message}
                    onChange={(e) => setMessage(e.target.value)}
                  />
                </div>
              </div>
              <div className="shop-detail__form__inputs-middle__section_third">
                <div className="shop-detail__form__inputs-label__section">
                  <label htmlFor="price">Hodnota vozu:</label>
                  <input
                    id="price"
                    type="text"
                    placeholder={price}
                    value={price}
                    onChange={(e) => setPrice(e.target.value)}
                  />
                </div>
              </div>
            </div>
            <div className="shop-detail__form__inputs-section">
              <div className="shop-detail__form__inputs-third__section-first">
                <div className="shop-detail__form__inputs-label__section">
                  <label htmlFor="city">Lokace</label>
                  <input
                    id="city"
                    type="text"
                    placeholder="Beroun"
                    value={city}
                    onChange={(e) => setCity(e.target.value)}
                  />
                </div>
              </div>
              <div className="shop-detail__form__inputs-third__section-second"></div>
            </div>
            <h3 className="section-name">Poškození / opotřebení vozidla</h3>
           {/*  {damageList.map((singleDamageInput, index) => (
              <div key={index}>
                <div className="shop-detail__form__inputs-section__fourth">
                  <div className="shop-detail__form__inputs-fourth__section-first">
                    <div className="shop-detail__form__inputs-label__section">
                      <input
                        type="text"
                        value={damage}
                        onChange={(e) => setDamage(e.target.value)}
                      />
                    </div>
                  </div>
                  <div className="shop-detail__form__inputs-fourth__section-second">
                    <div className="shop-detail__form__inputs-label__section">
                      <input
                        type="text"
                        placeholder="Lorem ipsum dolor est, segitur vivid inam"
                        value={damageText}
                        onChange={(e) => setDamageText(e.target.value)}
                      />
                    </div>
                  </div>
                  <div></div>
                </div>
                {damageList.length - 1 === index && (
                  <div className="shop-detail__form__inputs-fourth__section__new-damage">
                    <div className="shop-detail__form__inputs-fourth__section-first">
                      <div className="shop-detail__form__inputs-label__section">
                        <div className="input-icon__wrapper">
                          <input
                            type="text"
                            placeholder="Přidat další"
                            onClick={addNewDamageInput}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="shop-detail__form__inputs-fourth__section-second">
                      <div className="shop-detail__form__inputs-label__section">
                        <input
                          type="text"
                          placeholder="Popište, prosím, poškození"
                          onClick={addNewDamageInput}
                        />
                      </div>
                    </div>
                    <div></div>
                  </div>
                )}
              </div>
            ))} */}
            <div className="shop-detail__button__section">
              <button
                className="shop-detail__button__section-button"
                type="submit"
              >
                Odeslat
              </button>
            </div>
          </form>
{/*         ) : (
          <form>
            <div className="shop-detail__form__inputs-big__section">
              <input
                type="text"
                placeholder="WIN KÓD VAŠEHO VOZU NEBO SPZ"
                value={win}
                onChange={(e) => setWin(e.target.value)}
              />
            </div>
            <div className="shop-detail__button__section">
              <button
                className="shop-detail__button__section-button"
                type="submit"
              >
                Odeslat
              </button>
            </div>
          </form>
        ) */}
      </div>
      <div></div>
    </div>
  );
};

export default Advertisment;
