import React, { useState } from "react";

import "./ShopDetail.css";
import images from "../constants/images";
import { FaPlus } from "react-icons/fa";

const ShopDetail = () => {
  const [win, setWin] = useState("");
  const [kilometers, setKilometers] = useState("");
  const [brand, setBrand] = useState("");
  const [model, setModel] = useState("");
  const [year, setYear] = useState("");
  const [info, setInfo] = useState("");
  const [name, setName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [city, setCity] = useState("");
  const [message, setMessage] = useState("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam erat volutpat. \
     Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Etiam neque. Cras pede libero, \
     dapibus nec, pretium sit amet, tempor quis. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Proin mattis lacinia justo. Aliquam in lorem sit amet leo accumsan lacinia.\
      Praesent dapibus. Maecenas aliquet accumsan leo. Duis condimentum augue id magna semper rutrum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. \
      Aenean id metus id velit ullamcorper pulvinar. Quisque porta. Nulla pulvinar eleifend sem. Phasellus et lorem id felis nonummy placerat. Neque porro quisquam est, \
      qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. \
      Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?");
  const [price, setPrice] = useState("1.499.000");
  const [tax, setTax] = useState("75.000");
  const [usage, setUsage] = useState("350.000");
  const [damagePrice, setDamagePrice] = useState("280.000");
  const [finalPrice, setFinalPrice] = useState("794.000");
  const [damage, setDamage] = useState("Typ poškození");
  const [damageText, setDamageText] = useState("");
  const [damageList, setDamageList] = useState([{damageInput: ''}])

  const handleSubmit = async (e) => {
    e.preventDefault();
  };

  const setYearInput = (e) => {
    setYear(e.target.value.replace(/[^\d]/g, ""));
  }

  const setKilometersInput = (e) => {
    setKilometers(e.target.value.replace(/[^\d]/g, ""));
  }

  const addNewDamageInput = () => {
    setDamageList([...damageList, {damageInput: ''}])
  }

  return (
    <div className="shop-detail__container">
      <div className="shop-detail__section-form">
        <div className="shop-detail__intro-wrapper">
          <h1>Výkup vozidel</h1>
          <p>
            Prosím vyplňte pečlivě následující tabulku, abychom mohli co
            nejpřesněji stanovit odhadovanou cenu
          </p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="shop-detail__form__inputs-section">
            <div className="shop-detail__form__inputs-upper__section-first">
              <div className="shop-detail__form__inputs-big__section">
                <input
                  type="text"
                  placeholder="WIN KÓD VAŠEHO VOZU NEBO SPZ"
                  value={win}
                  onChange={(e) => setWin(e.target.value)}
                />
              </div>
              <div className="shop-detail__form__inputs-small__section">
                <input
                  type="text"
                  id="brand"
                  placeholder="Značka"
                  value={brand}
                  onChange={(e) => setBrand(e.target.value)}
                />
                <input
                  type="text"
                  id="model"
                  placeholder="Model"
                  value={model}
                  onChange={(e) => setModel(e.target.value)}
                />
                <input
                  type="text"
                  id="year"
                  placeholder="Rok výroby"
                  value={year}
                  onChange={(e) => setYearInput(e)}
                />
              </div>
            </div>
            <div className="shop-detail__form__inputs-upper__section-second">
              <input
                type="text"
                id="kilometres"
                placeholder="Najeté Kilometry"
                value={kilometers}
                onChange={(e) => setKilometersInput(e)}
              />
              <input
                className="shop-detail__form__inputs-section-second__input"
                type="text"
                placeholder="Doplňující informace"
                value={info}
                onChange={(e) => setInfo(e.target.value)}
              />
            </div>
            <div className="shop-detail__form__inputs-upper__section-third">
              <div className="rectangular">
                <div className="rectangular-img">
                  <img src={images.wv} alt="car"></img> 
                </div>
              </div>
            </div>
          </div>
          <div className="shop-detail__form__inputs-section-middle">
            <div className="shop-detail__form__inputs-middle__section-first h-auto">
              <div className="shop-detail__form__inputs-label__section">
                <label htmlFor="name">Vaše celé jméno</label>
                <input
                  id="name"
                  type="text"
                  placeholder="Pavel Novák"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </div>
              <div className="shop-detail__form__inputs-label__section">
                <label htmlFor="phone">Telefon</label>
                <input
                  id="phone"
                  type="text"
                  placeholder="+420 731 695 368"
                  value={phoneNumber}
                  onChange={(e) => setPhoneNumber(e.target.value)}
                />
              </div>
              <div className="shop-detail__form__inputs-label__section">
                <label htmlFor="email">Email</label>
                <input
                  id="email"
                  type="text"
                  placeholder="NovakP@seznam.cz"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
            </div>
            <div className="shop-detail__form__inputs-middle__section_second">
              <div className="shop-detail__form__inputs-label__section h-100">
                <label htmlFor="message">
                  Zde nám zanechte zprávu či dotaz
                </label>
                <textarea 
                  className="h-100"
                  id="message"
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                />
              </div>
            </div>
            <div className="shop-detail__form__inputs-middle__section_third">
              <div className="shop-detail__form__inputs-label__section">
                <label htmlFor="price">Odhadovaná hodnota vozu:</label>  
                <input
                  id="price"
                  type="text"
                  placeholder={price}
                  value={price}
                  onChange={(e) => setPrice(e.target.value)}
                />
              </div>
              <div className="shop-detail__form__price-table">
                <div className="shop-detail__form__price-table__entry">
                  <p>Základní cena</p>
                  <strong>{price}</strong>
                </div>
                <div className="shop-detail__form__price-table__entry">
                  <p>Položka</p>
                  <strong>- {tax}</strong>
                </div>
                <div className="shop-detail__form__price-table__entry">
                  <p>Opotřebení</p>
                  <strong>- {usage}</strong>
                </div>
                <div className="shop-detail__form__price-table__entry">
                  <p>Požkození</p>
                  <strong>- {damagePrice}</strong>
                </div>
                <div className="shop-detail__form__price-table__entry result-entry">
                  <p>Odhadovaná výkupní cena</p>  
                  <strong>{finalPrice}</strong>
                </div> 
              </div> 
            </div>
          </div>
          <div className="shop-detail__form__inputs-section">
            <div className="shop-detail__form__inputs-third__section-first">
              <div className="shop-detail__form__inputs-label__section">
                <label htmlFor="city">Lokace</label>
                <input
                  id="city"
                  type="text"
                  placeholder="Beroun"
                  value={city}
                  onChange={(e) => setCity(e.target.value)}
                />
              </div>
            </div>
            <div className="shop-detail__form__inputs-third__section-second"></div>
            <div className="shop-detail__form__inputs-third__section-third">
              <p className="shop-detail__form__inputs-price-text">* Cena není konečná, konkrétní částka bude v cenové nabídce v emailu lorem ipsum dolor est segitur viviad sinit</p>
            </div>
          </div>
          <h3 className="section-name">Poškození / opotřebení vozidla</h3>
          {damageList.map((singleDamageInput, index) => (
            <div key={index}>
              <div className="shop-detail__form__inputs-section__fourth">
                <div className="shop-detail__form__inputs-fourth__section-first">
                  <div className="shop-detail__form__inputs-label__section">
                    <input 
                      type="text"
                      value={damage}
                      onChange={(e) => setDamage(e.target.value)}
                    />
                  </div>
                </div>
                <div className="shop-detail__form__inputs-fourth__section-second">
                  <div className="shop-detail__form__inputs-label__section">
                    <input 
                      type="text"
                      placeholder="Lorem ipsum dolor est, segitur vivid inam"
                      value={damageText}
                      onChange={(e) => setDamageText(e.target.value)}
                    />
                  </div>
                </div>
                <div></div>
              </div>
              {damageList.length - 1 === index &&
                <div className="shop-detail__form__inputs-fourth__section__new-damage">
                  <div className="shop-detail__form__inputs-fourth__section-first">
                    <div className="shop-detail__form__inputs-label__section">
                      <div className="input-icon__wrapper">
                        <input 
                          type="text"
                          placeholder="Přidat další"
                          onClick={addNewDamageInput}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="shop-detail__form__inputs-fourth__section-second">
                    <div className="shop-detail__form__inputs-label__section">
                      <input 
                        type="text"
                        placeholder="Popište, prosím, poškození"
                        onClick={addNewDamageInput}
                      />
                    </div>
                  </div>
                  <div></div>
                </div>
              }
            </div>
            ))}
          <div className="shop-detail__button__section">
            <button 
              className="shop-detail__button__section-button"
              type="submit"
            >
              Odeslat
            </button>
          </div>
        </form>
      </div>
      <div></div>
    </div>
  );
};

export default ShopDetail;
