import React from "react";
import { Link } from "react-router-dom";
import { useState, useRef } from "react";
import { BsFillArrowRightCircleFill } from "react-icons/bs";
import SelectSearch, { fuzzySearch } from 'react-select-search';
import { useDispatch, useSelector } from "react-redux";

import images from "../constants/images";
import "./Shop.css";
import "./SelectSearch.css";
import CarState from "../Components/CarState";
import Price from "../Components/Price";
import { useEffect } from "react";
import { getAllAds } from "../actions/AdvertisingActions";
import LoadingBox from "../Components/LoadingBox";
import MessageBox from "../Components/MessageBox";

const Shop = () => {
/*   const searchInput = useRef();
  const [price, setPrice] = useState("1.150.000,-");
  const [brand, setBrand] = useState(""); */

  const dispatch = useDispatch();
  const adsList = useSelector((state) => state.allAds);
  const { loading, error, ads } = adsList;


  useEffect(() => {
    dispatch(getAllAds());
  }, [dispatch]);

  return (
    <div className="shop__container">
      <div className="shop__filter">
{/*         <div>
            <SelectSearch
                ref={searchInput} 
                options={brandOptions} 
                search 
                name='brands' 
                placeholder="Hledat značku" 
                filterOptions={fuzzySearch}
                value={brand}
                onChange={setBrand}  
            />
            {brand !== '' &&
                <SelectSearch
                    options={modelOptions}
                    search
                    name='models'
                    placeholder="Hledat model"
                    filterOptions={fuzzySearch}
                />
            }
        </div>
        <CarState />
        <Price /> */}
      </div>
      <div className="shop__content">
{/*         {loading ? (
          <LoadingBox></LoadingBox>
        ) : error ? (
          <MessageBox variant="danger">{error}</MessageBox>
        ) : ( */}
          {ads != undefined ? ( ads.map((singleAd) => (
          <div className="shop__carLayout">
              <img src={images.wv} alt="car"></img>
              <div className="shop__carLayout-info">
              <h3>{singleAd.name}</h3>
              <p>{`Osobní - 140 kW - ${singleAd.palivo} - ${singleAd.prevodovka}`}</p>
              <div className="shop__carLayout-action__wrapper">
                <div>
                  <Link to={`/shop/${singleAd._id}`}>Detail</Link>
                  <BsFillArrowRightCircleFill />
                </div>
                <strong>{singleAd.cena}</strong>
              </div>
            </div>
          </div>
        ))) : <div />}
      </div>
      <div className="shop__ad">

      </div>
    </div>
  );
};

export default Shop;
