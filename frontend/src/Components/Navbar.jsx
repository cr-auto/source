import React from "react";

import "./Navbar.css";
import images from "../constants/images";

const Navbar = () => {
  return (
    <div className="Navbar__Header-wrapper">
      <div className="Navbar__Header-img">
        <img src={images.logo} alt="logo" />
      </div>
      <ul className="Navbar__Header-list">
        <li>Pronájem</li>
        <li>Prodej a výkup</li>
        <li>Inzerce</li>
        <li>Kontakty</li>
        <li>O nás</li>
        <li>Můj účet</li>
      </ul>
    </div>
  );
};

export default Navbar;
