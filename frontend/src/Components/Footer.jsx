import React from 'react'
import { FaFacebook, FaInstagram } from 'react-icons/fa'

import "./Footer.css";

const Footer = () => {
  return (
    <div className='footer__container'>
      <div className='footer__wrapper'>
        <h1>Kontakt</h1>
        <p className='footer__desc-text'>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aliquam erat volutpat. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. 
            Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Etiam neque. Cras pede libero, dapibus nec, 
            pretium sit amet, tempor quis. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Proin mattis lacinia justo. 
            Aliquam in lorem sit amet leo accumsan lacinia 
        </p>
        <h2>EUSTUFF s.r.o.</h2>
        <p className='footer__info-text'>
            Rybná 716/24, Staré Město, 110 00 Praha 1 <br />
            IČO: 119 41 658 <br />
            +420 722 200 566 <br />
            info@autopujcovnacr.com <br />
            rezervace@autopujcovnacr.com
        </p>
        <div className='footer__links'>
            <a href="facebook.com">Facebook <FaFacebook /></a>
            <a href="instagram.com">Instagram <FaInstagram /></a>
        </div>
      </div>
      <div className='footer__map'>

      </div>
      <div className='footer__logo'>
        
      </div>
    </div>
  )
}

export default Footer
