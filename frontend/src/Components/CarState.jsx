import React, { useState } from 'react';

import "./CarState.css";

const CarState = () => {
    const [newCar, setNewCar] = useState(true);
    const [used, setUsed] = useState(true);
    const [demo, setDemo] = useState(true);
    const [crashed, setCrashed] = useState(false);
    const [veteran, setVeteran] = useState(false);

  return (
    <div className='car__state__wrapper'>
        <h3>Stav vozidla</h3>
        <div>
            <input type="checkbox" id="new" checked={newCar} onChange={setNewCar} />
            <label htmlFor="new">Nové</label><br />        
            <input type="checkbox" id="used" checked={used} onChange={setUsed} /> 
            <label htmlFor="used">Ojeté</label>        <br />
            <input type="checkbox" id="demo" checked={demo} onChange={setDemo} />
            <label htmlFor="demo">Předváděcí</label>            <br />
            <input type="checkbox" id="crashed" checked={crashed} onChange={setCrashed} />
            <label htmlFor="crashed">Havarované</label>            <br />
            <input type="checkbox" id="veteran" checked={veteran} onChange={setVeteran} />
            <label htmlFor="veteran">Veterán</label>
        </div>
    </div>
  )
}

export default CarState
