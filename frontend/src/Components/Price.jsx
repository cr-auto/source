import React from 'react'
import { useState } from 'react'
import SelectSearch, { fuzzySearch } from 'react-select-search'

const Price = () => {
const [price1, setPrice1] = useState(false)
const [price2, setPrice2] = useState(false)
const [price3, setPrice3] = useState(false)
const [ownPrice, setOwnPrice] = useState(false)
const [priceFrom, setPriceFrom] = useState('')
const [priceTo, setPriceTo] = useState('')
const [leasing, setLeasing] = useState(false)
const [taxReduction, setTaxReduction] = useState(false)

    const prices = [
        {name:"15 000 Kč", value:15000},
        {name:"30 000 Kč", value:30000},
        {name:"50 000 Kč", value:50000},
        {name:"75 000 Kč", value:75000},
        {name:"100 000 Kč", value:100000},
        {name:"150 000 Kč", value:150000},
        {name:"200 000 Kč", value:200000},
        {name:"250 000 Kč", value:250000},
        {name:"300 000 Kč", value:300000},
        {name:"350 000 Kč", value:350000},
        {name:"400 000 Kč", value:400000},
        {name:"500 000 Kč", value:500000},
        {name:"750 000 Kč", value:750000},
        {name:"1 000 000 Kč", value:1000000},
        {name:"2 000 000 Kč", value:2000000},
        {name:"4 000 000 Kč", value:4000000},
    ];

  return (
    <div>
      <h3>Cena</h3>
      <div>
        <input type="radio" id="price1" checked={price1} onChange={(e) => setPrice1(e.target.checked)} name="prices" />
        <label htmlFor='price1'>Do 50 000 Kč</label> <br />
        <input type="radio" id="price2" checked={price2} onChange={(e) => setPrice2(e.target.checked)} name="prices" />
        <label htmlFor='price2'>Do 100 000 Kč</label> <br />
        <input type="radio" id="price3" checked={price3} onChange={(e) => setPrice3(e.target.checked)} name="prices" />
        <label htmlFor='price3'>Do 200 000 Kč</label> <br />
        <input type="radio" id="own" checked={ownPrice} onChange={(e) => setOwnPrice(e.target.checked)} name="prices" />
        <label htmlFor='own'>Vlastní</label> <br />
        {ownPrice === true && 
            console.log(ownPrice) &&
            <div>
                <SelectSearch 
                    options={prices}  
                    search
                    filterOptions={fuzzySearch}
                    value={priceFrom}
                    onChange={setPriceFrom}
                    placeholder='Od'
                />
                <SelectSearch 
                    options={prices}  
                    search
                    filterOptions={fuzzySearch}
                    value={priceTo}
                    onChange={setPriceTo}
                    placeholder='Do'
                />
            </div>
        }
        <input type="radio" id="leasing" checked={leasing} onChange={(e) => setLeasing(e.target.checked)} name="prices" />
        <label htmlFor='price3'>Cena na leasing</label> <br />
        <input type="radio" id="tax" checked={taxReduction} onChange={(e) => setTaxReduction(e.target.checked)} name="prices" />
        <label htmlFor='tax'>Možnost odpočtu DPH</label>
      </div>
    </div>
  )
}

export default Price
