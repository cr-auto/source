import { SHOP_CREATE_FAIL, SHOP_CREATE_REQUEST, SHOP_CREATE_RESET, SHOP_CREATE_SUCCESS } from "../constants/ShopConstants";

export const shopCreateReducer = (state = {}, action) => {
    switch(action.type) {
        case SHOP_CREATE_REQUEST:
            return { loading: true };
        case SHOP_CREATE_SUCCESS:
            return { loading: false, success: true, carAd: action.payload };
        case SHOP_CREATE_FAIL:
            return { loading: false, error: action.payload };
        case SHOP_CREATE_RESET:
            return {};
        default:
            return state;
    }
};