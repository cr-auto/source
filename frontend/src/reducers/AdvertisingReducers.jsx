import { ADVERTISING_CREATE_FAIL, ADVERTISING_CREATE_REQUEST, ADVERTISING_CREATE_RESET, ADVERTISING_CREATE_SUCCESS, ADVERTISING_GET_ALL_FAIL, ADVERTISING_GET_ALL_REQUEST, ADVERTISING_GET_ALL_SUCCESS, GET_BRANDS_FAIL, GET_BRANDS_REQUEST, GET_BRANDS_SUCCESS, GET_MODELS_FAIL, GET_MODELS_REQUEST, GET_MODELS_SUCCESS } from "../constants/AdvertisingConstants";

export const advCreateReducer = (state = {}, action) => {
    switch (action.type) {
        case ADVERTISING_CREATE_REQUEST:
            return { loading: true };
        case ADVERTISING_CREATE_SUCCESS:
            return { loading: false, success: true, adv: action.payload }
        case ADVERTISING_CREATE_FAIL:
            return { loading: false, error:  action.payload }
        case ADVERTISING_CREATE_RESET:
            return {};
        default:
            return state;
    }
};

export const getBrandsReducer = (state = {}, action) => {
    switch (action.type) {
        case GET_BRANDS_REQUEST:
            return { loading: true };
        case GET_BRANDS_SUCCESS:
            return { loading: false, data: action.payload }
        case GET_BRANDS_FAIL:
            return { loading: false, error:  action.payload }
        default:
            return state;
    }
};

export const getAllAdsReducer = (state = {}, action) => {
    switch (action.type) {
        case ADVERTISING_GET_ALL_REQUEST:
            return { loading: true };
        case ADVERTISING_GET_ALL_SUCCESS:
            return { loading: false, ads: action.payload }
        case ADVERTISING_GET_ALL_FAIL:
            return { loading: false, error: action.payload }
        default:
            return state;
    }
};

export const getModelsReducer = (state = {}, action) => {
    switch (action.type) {
        case GET_MODELS_REQUEST:
            return { loading: true };
        case GET_MODELS_SUCCESS:
            return { loading: false, data: action.payload }
        case GET_MODELS_FAIL:
            return { loading: false, error:  action.payload }
        default:
            return state;
    }
};