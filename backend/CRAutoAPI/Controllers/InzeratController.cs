﻿using DataModels;
using DataModels.DataContexts;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace CRAutoAPI.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class InzeratController : Controller
	{
		private readonly DataContext _context;

		public InzeratController(DataContext context)
		{
			_context = context;
		}

		private static List<Inzerat> inzeraty = new List<Inzerat>
			{
				new Inzerat {
				Id = 1,
				Znacka = "Škoda",
				NajeteKm = 120000,
				Cena = 500000
				},
				new Inzerat {
					Id = 2,
				Znacka = "Audi",
				NajeteKm = 150000,
				Cena = 600000
				}
			};

		[HttpGet("GetAllAds")]
		public async Task<ActionResult<List<Inzerat>>> GetAllAds()
		{
			var ads = _context.Inzeraty.ToList();
			return Ok(ads);
		}

		[HttpGet("GetAllBrands")]
		public async Task<ActionResult<List<Znacka>>> GetAllBrands()
		{
			var brands = _context.Znacky.ToList();
			return Ok(brands);
		}

		[EnableCors]
		[HttpPost("GetAllModelsForBrand")]
		public async Task<ActionResult<List<Znacka>>> GetAllModelsForBrand(int id)
		{
			var models = _context.Modely.Where(x => x.ZnackaVozu == id).ToList();
			return Ok(models);
		}

		[HttpGet("GetSingleAd/{id}")]
		public async Task<ActionResult<Inzerat>> GetAd(int id)
		{
			var inzerat = inzeraty.Find(i => i.Id == id);
			if (inzerat == null)
			{
				return BadRequest("Inzerát nenalezen.");
			}
			return Ok(inzerat);
		}

		[HttpPost("SaveAdTEST")]
		public async Task<ActionResult<Inzerat>> SaveAdTEST(Inzerat ad)
		{
			inzeraty.Add(ad);
			return Ok(inzeraty);
		}

		[EnableCors]
		[HttpPost("SaveAd")]
		public async Task<ActionResult<Inzerat>> SaveAd(Inzerat ad)
		{
			ModelState.Remove("Id");
			_context.Inzeraty.Add(ad);
			await _context.SaveChangesAsync();
			return Ok(GetAllAds());
		}

		[HttpPut("ModifyAd/{id}")]
		public async Task<ActionResult<List<Inzerat>>> PutAd(Inzerat ad)
		{
			var inzerat = inzeraty.Find(i => i.Id == ad.Id);
			if (inzerat == null)
			{
				return BadRequest("Inzerát nenalezen.");
			}
			inzerat.NajeteKm = ad.NajeteKm;
			inzerat.Cena = ad.Cena;
			inzerat.Znacka = ad.Znacka;

			return Ok(inzeraty);
		}

		[HttpDelete("DeleteAd/{id}")]
		public async Task<ActionResult<List<Inzerat>>> Delete(int id)
		{
			var inzerat = inzeraty.Find(i => i.Id == id);
			if (inzerat == null)
			{
				return BadRequest("Inzerát nenalezen.");
			}
			inzeraty.Remove(inzerat);
			return Ok(inzeraty);
		}
	}
}