import bcrypt from 'bcryptjs';

const data = {
    users: [
        {
            name: 'Filip',
            email: 'admin@admin.com',
            password: bcrypt.hashSync('1234', 8),
            isAdmin: true,
        },
    ],
}

export default data;