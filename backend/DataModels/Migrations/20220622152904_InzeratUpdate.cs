﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataModels.Migrations
{
    public partial class InzeratUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Inzeraty_Zakaznici_VlastnikId",
                table: "Inzeraty");

            migrationBuilder.DropIndex(
                name: "IX_Inzeraty_VlastnikId",
                table: "Inzeraty");

            migrationBuilder.RenameColumn(
                name: "NajeteKM",
                table: "Inzeraty",
                newName: "NajeteKm");

            migrationBuilder.RenameColumn(
                name: "VlastnikId",
                table: "Inzeraty",
                newName: "RokVyroby");

            migrationBuilder.AddColumn<string>(
                name: "AdMessage",
                table: "Inzeraty",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "DodatecneInfo",
                table: "Inzeraty",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Inzeraty",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Jmeno",
                table: "Inzeraty",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "Inzeraty",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Model",
                table: "Inzeraty",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Palivo",
                table: "Inzeraty",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Prevodovka",
                table: "Inzeraty",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "TelCislo",
                table: "Inzeraty",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdMessage",
                table: "Inzeraty");

            migrationBuilder.DropColumn(
                name: "DodatecneInfo",
                table: "Inzeraty");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Inzeraty");

            migrationBuilder.DropColumn(
                name: "Jmeno",
                table: "Inzeraty");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "Inzeraty");

            migrationBuilder.DropColumn(
                name: "Model",
                table: "Inzeraty");

            migrationBuilder.DropColumn(
                name: "Palivo",
                table: "Inzeraty");

            migrationBuilder.DropColumn(
                name: "Prevodovka",
                table: "Inzeraty");

            migrationBuilder.DropColumn(
                name: "TelCislo",
                table: "Inzeraty");

            migrationBuilder.RenameColumn(
                name: "NajeteKm",
                table: "Inzeraty",
                newName: "NajeteKM");

            migrationBuilder.RenameColumn(
                name: "RokVyroby",
                table: "Inzeraty",
                newName: "VlastnikId");

            migrationBuilder.CreateIndex(
                name: "IX_Inzeraty_VlastnikId",
                table: "Inzeraty",
                column: "VlastnikId");

            migrationBuilder.AddForeignKey(
                name: "FK_Inzeraty_Zakaznici_VlastnikId",
                table: "Inzeraty",
                column: "VlastnikId",
                principalTable: "Zakaznici",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
