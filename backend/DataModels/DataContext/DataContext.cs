﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModels.DataContexts
{
	public class DataContext : DbContext
	{
		public DataContext(DbContextOptions<DataContext> options) : base(options)
		{
		}

		public DbSet<Inzerat> Inzeraty { get; set; }
		public DbSet<Zakaznik> Zakaznici { get; set; }
		public DbSet<Znacka> Znacky { get; set; }
		public DbSet<Model> Modely { get; set; }
	}
}