﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModels
{
	public class Model
	{
		public int Id { get; set; }
		public string ModelVozu { get; set; }

		[ForeignKey("Znacka")]
		public int ZnackaVozu { get; set; }
	}
}