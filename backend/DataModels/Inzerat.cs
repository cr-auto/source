﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace DataModels
{
	public class Inzerat
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[JsonIgnore]
		public int Id { get; set; }

		public string Znacka { get; set; }
		public string Model { get; set; }
		public int Cena { get; set; }
		public int RokVyroby { get; set; }
		public int NajeteKm { get; set; }
		public string Palivo { get; set; }
		public string Prevodovka { get; set; }
		public string DodatecneInfo { get; set; }
		public string Jmeno { get; set; }
		public string Email { get; set; }
		public string TelCislo { get; set; }
		public string Location { get; set; }
		public string AdMessage { get; set; }
	}
}