﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModels
{
	public class Znacka
	{
		public int Id { get; set; }

		public string ZnackaVozu { get; set; }
	}
}